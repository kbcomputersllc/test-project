# PHP Interview Test

## PreProject Tasks

- Install [Composer](https://getcomposer.org/download/)
- Install [Laravel](https://laravel.com/docs/)

## Project Summary

Use the PHP framework Laravel along with any other composer package you see fit, to make an application that has a simple contact form with at least three fields.

- Name (input type=text)
- Email (input type=email)
- Message (textarea)

Upon clicking the submit button you should submit your form to a route that saves something to an SQLite database. That way it will create a new row in your table. Use the model event `created` to send the email.

This project should also have a "splash screen" so when the user first loads the page they are greeted with a beautiful screen, and information about who you are. 
It would be totally okay if you use a template so long as you have the proper rights for the it. You can expand upon this website and make it your own personal site, so long as what is submitted meets the requirements.

This project should also have a screen thanking the user for their submission.

## Instructions

Do your development in a local git repository and commit your changes as you're
developing the project.

Use php 7.0's built-in server through 'artisan' for local development.
["Serving Laravel" section](http://laravel.com/docs/)

Feel free to google what you need to get the task done, but please, don't copy their code verbatim and write your own code.

If you have any questions feel free to contact me at one of the emails below

- austinkregel@gmail.com [preferred]
- akregel@saycomputer.net [other preffered]
- me@austinkregel.com 
- Or reach out to me on [hipchat](https://kbcomp.hipchat.com/invite/346174/0222e2a55e3a73d855e86da78ba9961f)

## Goals (not required but nice)
- Try and write [semantic markup in your templates](http://webdesignerwall.com/tutorials/coding-clean-and-semantic-templates).
- Remember [separation of concerns](http://en.wikipedia.org/wiki/Separation_of_concerns) when developing in an [MVC framework](http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) such as Laravel.
- Try and complete in 5 days.
- Consider [input sanitization](http://laravelbook.com/laravel-input-validation/).
- Keep the [DRY programming concepts](http://en.wikipedia.org/wiki/Don't_repeat_yourself) in mind
- [Input validation](http://laravel.com/docs/validation) for your form (You want to make sure that the email addresses typed are email addresses and not something without an '@' symbol.
- Keep your code clean by following the ["returning early pattern"](https://en.wikibooks.org/wiki/Computer_Programming/Coding_Style/Minimize_nesting#Early_return)
- Follow [SOLID](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)) design principles.

Also, if you have time, please look into the following

- Using [Eloquent Model events](https://laravel.com/docs/eloquent#events)
- Using [Laravel's migration](https://laravel.com/docs/migrations) system
- Using [NodeJS](https://nodejs.org), [socket.io](http://socket.io), and [Redis](http://redis.io) (won't be required until November)
- Using [Laravel's Events](https://laravel.com/docs/events)

Resources:
http://laravel.com/docs/
http://laracasts.com (This is the BEST resource you will ever have for Laravel.) (totally not bias or anything... ;) )